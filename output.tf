output "project_id" {
  value = "${google_project.gitlab_project.id}"
}

output "dns_servers" {
  value = "${google_dns_managed_zone.gitlab_zone.name_servers}"
}
