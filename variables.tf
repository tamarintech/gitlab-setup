variable "project_region" {
  default = "us-central1"
}

variable "project_zone" {
  default = "us-central1-a"
}

variable "kubernetes_node_count" {
  default = 4
}

variable "kubernetes_node_preempt" {
  default = true
}

variable "kubernetes_node_type" {
  default = "n1-standard-4"
}

variable "dns_zone_name" {}

variable "database_secret" {}
variable "project_billing" {}
variable "project_folder" {}
variable "certmanager_issuer" {}
