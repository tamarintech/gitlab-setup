resource google_container_cluster "container_cluster" {
  name                     = "gcp-container-cluster"
  project                  = "${google_project.gitlab_project.id}"
  location                 = "${var.project_zone}"
  remove_default_node_pool = true
  initial_node_count       = 1
  network                  = "${google_compute_network.gitlab_network.self_link}"

  # No longer necessary as of April. Set to False to disable VPC Native Networking
  ip_allocation_policy {
    use_ip_aliases = true
  }
}

resource "google_container_node_pool" "node_pool" {
  name       = "gcp-container-node-pool-1"
  location   = "${var.project_zone}"
  project    = "${google_project.gitlab_project.id}"
  cluster    = "${google_container_cluster.container_cluster.id}"
  node_count = "${var.kubernetes_node_count}"

  node_config {
    preemptible  = "${var.kubernetes_node_preempt}"
    machine_type = "${var.kubernetes_node_type}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

provider "kubernetes" {
  version                = "1.5.2"
  host                   = "${google_container_cluster.container_cluster.endpoint}"
  username               = "${google_container_cluster.container_cluster.master_auth.0.username}"
  password               = "${google_container_cluster.container_cluster.master_auth.0.password}"
  cluster_ca_certificate = "${base64decode(google_container_cluster.container_cluster.master_auth.0.cluster_ca_certificate)}"
}

resource "kubernetes_service_account" "tiller_service_account" {
  metadata {
    name      = "tiller"
    namespace = "kube-system"
  }
}

resource "kubernetes_cluster_role_binding" "tiller_cluster_role_binding" {
  metadata {
    name = "tiller"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = "tiller"
    namespace = "kube-system"
  }

  depends_on = ["kubernetes_service_account.tiller_service_account"]
}

provider "helm" {
  version         = "~> 0.9.0"
  service_account = "tiller"
  namespace       = "kube-system"

  kubernetes {
    host                   = "${google_container_cluster.container_cluster.endpoint}"
    username               = "${google_container_cluster.container_cluster.master_auth.0.username}"
    password               = "${google_container_cluster.container_cluster.master_auth.0.password}"
    cluster_ca_certificate = "${base64decode(google_container_cluster.container_cluster.master_auth.0.cluster_ca_certificate)}"
  }
}

resource "kubernetes_namespace" "gitlab_namespace" {
  metadata {
    name = "mygitlab"
  }
}

resource "kubernetes_secret" "gitlab_postgresql_secret" {
  metadata {
    name      = "mygitlab-database"
    namespace = "${kubernetes_namespace.gitlab_namespace.id}"
  }

  data {
    username = "${google_sql_user.gitlab_sql_user.name}"
    password = "${google_sql_user.gitlab_sql_user.password}"
  }
}

data "helm_repository" "gitlab_repo" {
  name = "gitlab"
  url  = "https://charts.gitlab.io/"
}

resource "helm_release" "gitlab" {
  name       = "mygitlab"
  chart      = "gitlab"
  version    = "1.7.5"
  namespace  = "${kubernetes_namespace.gitlab_namespace.id}"
  repository = "${data.helm_repository.gitlab_repo.url}"
  keyring    = ""
  timeout    = "2400"

  values = [<<EOF
postgresql:
  install: false

global:
  hosts:
    domain: "${var.dns_zone_name}"
    externalIP: "${google_compute_address.gitlab_address.address}"
  psql:
    host: "${google_sql_database_instance.gitlab_db.private_ip_address}"
    username: "${google_sql_user.gitlab_sql_user.name}"
    database: "${google_sql_database.gitlab_database.name}"
    password:
      secret: "${kubernetes_secret.gitlab_postgresql_secret.metadata.0.name}"
      key: password
certmanager-issuer:
  email: "${var.certmanager_issuer}"
EOF
  ]

  depends_on = [
    "kubernetes_cluster_role_binding.tiller_cluster_role_binding",
    "kubernetes_secret.gitlab_postgresql_secret",
  ]
}
